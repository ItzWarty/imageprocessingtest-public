﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ImageProcessing
{
    unsafe class GoalFinder
    {

        /// <summary>
        /// distance from the bottom of the 3pt goal to the ground in inches
        /// </summary>
        public const double GOAL_ELEVATION = 104.125;

        /// <summary>
        /// the height of the goal from bottom to top
        /// </summary>
        public const double GOAL_HEIGHT = 12;

        /// <summary>
        /// the width of the goal from left to right
        /// </summary>
        public const double GOAL_WIDTH = 54;

        /// <summary>
        /// Find the goals and their locations
        /// </summary>
        /// <param name="pScan0">pointer to the first pixel of the image</param>
        /// <param name="stride">image stride</param>
        /// <param name="imageWidth">image width</param>
        /// <param name="imageHeight">image height</param>
        public static void FindGoals(byte* pScan0, int stride, int imageWidth, int imageHeight)
        {
            List<List<Point>> blobs;
            PixelTester.PixelTest test = PixelTester.TestGoalPixel;
            PixelFiller.PixelFill fill = PixelFiller.FillPixel;

            var states = new byte[imageWidth * imageHeight];
            fixed (byte* pStates = states)
            {
                //fill states
                FilterUtil.FilterLuminosity(pScan0, stride, pStates, imageWidth, imageHeight);

                //find blobs
                blobs = ProcessingUtil.FindBlobs(pStates, imageWidth, imageHeight, fill, test);
            }

            //draw corners and find locations
            //            bool a = false;

            foreach (var blob in blobs)
            {
                //find convex hull
                List<Point> chull = ProcessingUtil.ConvexHull(blob);

                if (chull.Count > 3)
                {
                    //draw blob corners
                    var corners = ProcessingUtil.FindCorners(chull);
                    foreach (var p in corners)
                    {
                        Console.WriteLine(p.X + " " + p.Y);
                        //*(uint*)(pScan0 + stride * p.Y + p.X * 4) = 0xFFFF0000U;
                        GraphicsUtil.DrawRectangle(pScan0, stride, imageWidth, imageHeight, p.X, p.Y, 1, 1, 0xFFFF0000);
                    }

                    //find locations

                    if (corners.Count == 4)
                    {
                        //find average height/width (dist between corners)
                        var avgW = (MathUtil.GetDistance(corners[0], corners[3]) + MathUtil.GetDistance(corners[1], corners[2])) / 2; // pixels
                        var avgH = (MathUtil.GetDistance(corners[0], corners[1]) + MathUtil.GetDistance(corners[2], corners[3])) / 2; // pixels
                        var centerX = (corners[0].X + corners[1].X + corners[2].X + corners[3].X) / 4.0;
                        var rotAngle = (centerX - imageWidth / 2) * GraphicsUtil.H_FOV;
                        Console.WriteLine("rotAngle: " + rotAngle);
                        Console.WriteLine("avgh: " + avgH);
                        Console.WriteLine("image Height: " + imageHeight);
                        var theta = avgH / imageHeight * GraphicsUtil.H_FOV * imageHeight / imageWidth; //pixels/inches*radians = radians?        derp.
                        Console.WriteLine("VFOV: " + GraphicsUtil.V_FOV);
                        Console.WriteLine("theta: " + theta + " (" + theta * 180 / Math.PI + ")");
                        var costheta = Math.Cos(theta);
                        var sintheta = Math.Sin(theta);
                        //                        Console.WriteLine("cos: " + costheta);
                        //                        Console.WriteLine("sin: " + sintheta);
                        Console.WriteLine("corner: " + corners[3].X + " " + corners[3].Y);
                        var dist = .5 / sintheta *
                                      (GOAL_HEIGHT * costheta +
                                       Math.Sqrt(GOAL_HEIGHT * GOAL_HEIGHT * costheta * costheta -
                                                 4 * GOAL_ELEVATION * (GOAL_ELEVATION + GOAL_HEIGHT) * sintheta * sintheta));
                        var yposp = avgW - imageWidth / 2;
                        var ypos = GOAL_WIDTH * avgW / yposp;
                        var zpos = GOAL_ELEVATION + GOAL_HEIGHT / 2;
                        var xpos = Math.Sqrt(dist * dist - ypos * ypos);
                        Console.WriteLine(xpos + " " + ypos + " " + zpos);
                        //                        DrawRectangle(pScan0,stride,imageWidth,imageHeight,corners[2].X,corners[2].Y,(int)avgH,(int)avgW,a?GREEN:BLUE);
                        //                        Console.WriteLine("dist: " + dist);
                        //                        Console.WriteLine(a ? "GREEN" : "BLUE");
                        //                        a = !a;
                    }

                    Console.WriteLine();
                }
            }
        }
    }
}
