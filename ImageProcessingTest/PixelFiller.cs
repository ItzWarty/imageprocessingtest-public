﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing
{
    unsafe class PixelFiller
    {
        /// <summary>
        /// marks the given pixel as filled
        /// </summary>
        /// <param name="pState">the pixel to fill</param>
        public delegate void PixelFill(byte* pState);

        public static void FillPixel(byte* pState)
        {
            *pState = (byte)PixelState.Visited;
        }

        public static void FillNullPixel(byte* pState)
        {
            *pState = (byte)PixelState.NullVisited;
        }

        public static void FillColoredPixel(byte* pState)
        {
            *pState = (byte)PixelState.Visited;
        }
    }
}
