﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Windows.Forms;

namespace ImageProcessing {
    internal enum PixelState : byte {

        Seed,
        Candidate,
        None,
        White,
        Red,
        Blue,
        Visited,
        NullVisited,

    }

    internal unsafe class Program {

        

        /// <summary>
        /// the path of the static image
        /// </summary>
        public const string IMAGE_PATH = "Resources/photo 4.jpg";


        // ReSharper disable UnusedParameter.Local
        private static void Main(string[] args) {
        // ReSharper restore UnusedParameter.Local

            //set the title of the console
            Console.Title = IMAGE_PATH;

            //get the image
            var image = (Bitmap)Image.FromFile(IMAGE_PATH);
            var imageData = image.LockBits(
                    new Rectangle(0, 0, image.Width, image.Height),
                    ImageLockMode.ReadWrite,
                    PixelFormat.Format32bppRgb
                    );
            var pScan0 = (byte*)imageData.Scan0.ToPointer();
            var stride = imageData.Stride;

            //start timer
            var startTime = DateTime.Now;

            //process image
//            FindGoals(pScan0, stride, image.Width, image.Height);
            FrisbeeFinder.FindFrisbees(pScan0, stride, image.Width, image.Height);

            //stop timer
            var runTime = DateTime.Now - startTime;

            Console.WriteLine("Runtime: " + runTime.TotalMilliseconds);
            image.UnlockBits(imageData);

            //make window
            var f = new Form {Text = IMAGE_PATH, ClientSize = image.Size};
            f.Paint += (s, e) => e.Graphics.DrawImage(image, 0, 0,image.Width,image.Height);
            f.MouseClick += (s, e) => Console.WriteLine(e.X + " " + e.Y);

            //start
            Application.Run(f);
        }

    }
}