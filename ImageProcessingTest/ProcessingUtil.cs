﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ImageProcessing
{
    unsafe class ProcessingUtil
    {
        /// <summary>
        /// return all blobs for the given test and mark them as filled
        /// </summary>
        /// <param name="pStates">pointer to the beginning of the array of pixel states</param>
        /// <param name="imageWidth">image width</param>
        /// <param name="imageHeight">image height</param>
        /// <param name="fill">the fill to use to fill pixels that past test</param>
        /// <param name="test">the test to use to test whether to fill pixel</param>
        /// <returns></returns>
        public static List<List<Point>> FindBlobs(byte* pStates, int imageWidth, int imageHeight,
                                                   PixelFiller.PixelFill fill, PixelTester.PixelTest test)
        {
            var blobs = new List<List<Point>>();

            for (var yOffset = 0; yOffset < imageHeight; yOffset++)
            {
                var pCurrentState = pStates + yOffset * imageWidth;
                for (var xOffset = 0; xOffset < imageWidth; xOffset++)
                {
                    if (test(*pCurrentState))
                    {
                        var currList = new List<Point>();
                        var count = FloodFill(xOffset, yOffset, currList, pStates, imageWidth, imageHeight, fill, test);
                        if (currList.Count > 100 && (test != PixelTester.TestGoalPixel || count[1] * 1.0 / count[0] < .01))
                            blobs.Add(currList);
                    }

                    pCurrentState++;
                }
            }

            return blobs;
        }

        /// <summary>
        /// fill the blob containing the given point
        /// </summary>
        /// <param name="x">x-coordinate of the point to fill</param>
        /// <param name="y">y-coordinate of the point to fill</param>
        /// <param name="blob">blob to add to</param>
        /// <param name="pStates">pointer to the start of the array of pixel states</param>
        /// <param name="imageWidth">image width</param>
        /// <param name="imageHeight">image height</param>
        /// <param name="fill">the fill to use to fill pixels that past test</param>
        /// <param name="test">the test to use to test whether to fill pixel</param>
        private static int[] FloodFill(
                int x, int y, ICollection<Point> blob,
                byte* pStates, int imageWidth, int imageHeight,
                PixelFiller.PixelFill fill, PixelTester.PixelTest test)
        {
            var pCurrentState = pStates + y * imageWidth + x;
            var count = new int[2];
            if (test(*pCurrentState))
            {
                var leftX = x - 1;
                var rightX = x + 1;
                var pLeftState = pCurrentState - 1;
                var pRightState = pCurrentState + 1;
                fill(pCurrentState);
                while (leftX >= 0 && test(*pLeftState))
                {
                    if (test == PixelTester.TestGoalPixel)
                    {
                        if (*(uint*)pLeftState == (byte)PixelState.Seed)
                            count[0]++;
                        else if (*(uint*)pLeftState == (byte)PixelState.Candidate)
                            count[1]++;
                    }
                    fill(pLeftState);
                    pLeftState--;
                    leftX--;
                }
                while (rightX < imageWidth && test(*pRightState))
                {
                    if (test == PixelTester.TestGoalPixel)
                    {
                        if (*(uint*)pRightState == (byte)PixelState.Seed)
                            count[0]++;
                        else if (*(uint*)pRightState == (byte)PixelState.Candidate)
                            count[1]++;
                    }
                    fill(pRightState);
                    pRightState++;
                    rightX++;
                }
                blob.Add(new Point(leftX + 1, y));
                if (leftX != rightX)
                    blob.Add(new Point(rightX, y));

                var count1 = new int[2];
                var count2 = new int[2];
                for (var i = leftX + 1; i < rightX; i++)
                {
                    if (y > 0)
                    {
                        count1 = FloodFill(
                                i, y - 1, blob,
                                pStates, imageWidth, imageHeight,
                                fill, test);
                    }
                    if (y < imageHeight - 1)
                    {
                        count2 = FloodFill(
                                i, y + 1, blob,
                                pStates, imageWidth, imageHeight,
                                fill, test);
                    }
                    count[0] += count1[0] + count2[0];
                    count[1] += count1[1] + count2[1];
                }
            }
            return count;
        }

        /// <summary>
        /// returns a list of the outside points of the blob
        /// </summary>
        /// <param name="blob">the blob whose convex hull to find</param>
        /// <returns>a list of the outside points of the blob</returns>
        public static List<Point> ConvexHull(List<Point> blob)
        {
            var bottomMost = new Point(0, 0);
            foreach (var p in blob)
            {
                if (bottomMost.Y < p.Y)
                    bottomMost = p;
                else if (bottomMost.Y == p.Y && bottomMost.X > p.Y)
                    bottomMost = p;
            }
            var chull = new List<Point> { bottomMost };
            var pnow = bottomMost;
            var pprev = new Point(pnow.X, -1);
            var pnext = new Point();
            while (true)
            {
                double dmin = 2;
                for (var i = 0; i < blob.Count; i++)
                {
                    if (blob[i].Equals(pnow))
                        continue;
                    var d = MathUtil.GetAngle(pprev, pnow, blob[i]);
                    if (!(d < dmin)) continue;
                    dmin = d;
                    pnext = blob[i];
                }
                if (pnext.Equals(chull[0]))
                    break;
                chull.Add(pnext);
                pprev = pnow;
                pnow = pnext;
            }
            return chull;
        }

        /// <summary>
        /// returns the corners of the given convex hull from the bottom-left counterclockwise
        /// (bl,tl,tr,br)
        /// </summary>
        /// <param name="chull">the convex hull used to find corners</param>
        /// <returns>the corners of the convex hull</returns>
        public static List<Point> FindCorners(List<Point> chull)
        {
            const double tolerance = 0.97;
            var prev = chull[chull.Count - 1];
            var curr = chull[0];
            var next = chull[1];
            var corners = new List<Point> { };
            for (var i = 0; i < chull.Count; i++)
            {
                if (MathUtil.GetCosAngle(prev, curr, next) < tolerance)
                    corners.Add(curr);
                prev = curr;
                curr = next;
                next = chull[(i + 2) % chull.Count];

            }
            //first pass for angles between points
            //check angle of last point using the point before and the first point
            /*if (corners.Count>=2 && GetCosAngle(corners[corners.Count - 2], corners[corners.Count - 1], corners[0]) < tolerance) {
                corners[0] = corners[corners.Count - 1];
                corners.RemoveAt(corners.Count - 1);
            }*/
            //second pass for distance from next point
            var realcorners = new List<Point>();
            for (var i = 0; i < corners.Count - 1; i++)
            {
                if (MathUtil.GetDistance(corners[i], corners[i + 1]) > 10)
                {
                    realcorners.Add(corners[i]);
                }
            }
            if (MathUtil.GetDistance(corners[corners.Count - 1], corners[0]) > 10)
                realcorners.Add(corners[corners.Count - 1]);
            //}
            return realcorners;
        }
    }
}
