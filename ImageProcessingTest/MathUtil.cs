﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ImageProcessing
{
    unsafe class MathUtil
    {
        /// <summary>
        /// angle of prev-now-next in radians(?)
        /// </summary>
        /// <param name="prev"></param>
        /// <param name="now"></param>
        /// <param name="next"></param>
        /// <returns>the angle prev-now-next</returns>
        public static double GetAngle(Point prev, Point now, Point next)
        {
            double x1 = prev.X;
            double y1 = prev.Y;
            double x2 = now.X;
            double y2 = now.Y;
            double x3 = next.X;
            double y3 = next.Y;
            return (((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)) + ((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2)) -
                    ((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1))) /
                    (2 * Math.Sqrt(((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)) * ((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2))));
        }

        /// <summary>
        /// returns the cosine of the angle prev-now-next
        /// </summary>
        /// <param name="prev"></param>
        /// <param name="now"></param>
        /// <param name="next"></param>
        /// <returns>the cosine of the angle prev-now-next</returns>
        public static double GetCosAngle(Point prev, Point now, Point next)
        {
            return Dot(prev, now, next) / (GetDistance(prev, now) * GetDistance(now, next));
        }

        /// <summary>
        /// returns the dot of prev-now and now-next
        /// </summary>
        /// <param name="prev"></param>
        /// <param name="now"></param>
        /// <param name="next"></param>
        /// <returns>the dot of prev-now and now-next</returns>
        private static double Dot(Point prev, Point now, Point next)
        {
            var v1 = new Point(now.X - prev.X, now.Y - prev.Y);
            var v2 = new Point(next.X - now.X, next.Y - now.Y);
            return v1.X * v2.X + v1.Y * v2.Y;
        }

        /// <summary>
        /// returns the distance between the two points
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns>the distance between the two points</returns>
        public static double GetDistance(Point first, Point second)
        {
            var diff = new Point(first.X - second.X, first.Y - second.Y);
            return Math.Sqrt(diff.X * diff.X + diff.Y * diff.Y);
        }

        /// <summary>
        /// returns the horizontal distance across the image at a given distance from the camera in inches
        /// </summary>
        /// <param name="dist">the distance from the camera</param>
        /// <param name="w">the width of the image in pixels</param>
        /// <returns>the horizontal distance across the image (inches)</returns>
        private static double GetInchesPerPixels(double dist, int w)
        {
            return GraphicsUtil.H_FOV_INCHES * dist / w;
        }
    }
}
