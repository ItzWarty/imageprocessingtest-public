﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace ImageProcessing
{
    unsafe class GraphicsUtil
    {

        //color constants
        public const uint WHITE = 0xFFFFFFFF;
        public const uint GRAY  = 0xFF7F7F7F;
        public const uint BLACK = 0xFF000000;
        public const uint RED   = 0xFFFF0000;
        public const uint GREEN = 0xFF00FF00;
        public const uint BLUE  = 0xFF0000FF;


        //Camera constants
        /// <summary>
        /// the height of the green (frisbees) camera in inches
        /// TODO: Find actual value
        /// </summary>
        public const double GREEN_CAMERA_HEIGHT = 2;

        /// <summary>
        /// the height of the blue (goals) camera in inches
        /// TODO: Find actual value
        /// </summary>
        public const double BLUE_CAMERA_HEIGHT = 10;

        /// <summary>
        /// the ratio of width to height of our image
        /// </summary>
        public const double ASPECT_RATIO = 640 / 480;

        /// <summary>
        /// horizontal fov of the camersa in radians
        /// </summary>
        public const double H_FOV = 67 * Math.PI / 180;

        /// <summary>
        /// vertical fov of the cameras in radians
        /// </summary>
        public const double V_FOV = H_FOV / ASPECT_RATIO;

        /// <summary>
        /// the horizontal fov in terms of inches at 1in. away
        /// </summary>
        public const double H_FOV_INCHES = 76 / 71; // width(in.) / dist(in.)

        /// <summary>
        /// draws a solid (filled) rectangle
        /// </summary>
        /// <param name="pScan0">pointer to the first pixel of the image</param>
        /// <param name="stride">image stride</param>
        /// <param name="imageWidth">image width</param>
        /// <param name="imageHeight">image height</param>
        /// <param name="left">top left x-coordinate of the rectangle</param>
        /// <param name="top">top left y-coordinate of the rectangle</param>
        /// <param name="width">width of rectangle to draw</param>
        /// <param name="height">height of rectangle to draw</param>
        /// <param name="color">color to draw rectangle</param>
        public static void DrawRectangle(
                byte* pScan0, int stride, int imageWidth, int imageHeight,
                int left, int top, int width, int height, uint color)
        {
            var startX = Math.Max(0, left);
            var startY = Math.Max(0, top);
            var endX = Math.Min(left + width, imageWidth - 1);
            var endY = Math.Min(top + height, imageHeight - 1);

            for (var y = startY; y <= endY; y++)
            {
                var pScan = (uint*)(pScan0 + stride * y + startX * 4);
                for (var x = startX; x <= endX; x++, pScan++)
                    *(pScan) = color;
            }
        }

        //static void FlipRB(byte* pScan0, int stride, int w, int h)
        //{
        //    for (int xOffset = 0; xOffset < w; xOffset++)
        //    {
        //        for (int yOffset = 0; yOffset < h; yOffset++)
        //        {
        //            byte* pPixel = (byte*)(pScan0 + stride * yOffset + xOffset * 4);
        //            byte temp = pPixel[0];
        //            pPixel[0] = pPixel[2];
        //            pPixel[2] = temp;
        //        }
        //    }
        //}
    }
}
