﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ImageProcessing
{
    unsafe class FilterUtil
    {
        byte[] hsl = new byte[1 << 21];

        /// <summary>
        /// initializes the lookup table for RGB --> HSL conversion
        /// </summary>
        public static void InitializeLUT()
        {

        }


        /// <summary>
        /// filter the image by luminosity for goals
        /// </summary>
        /// <param name="pScan0">pointer to the first pixel of the image</param>
        /// <param name="imageStride">image stride</param>
        /// <param name="pStates">pointer to the array of pixel states</param>
        /// <param name="imageWidth">image width</param>
        /// <param name="imageHeight">image height</param>
        public static void FilterLuminosity(
                byte* pScan0, int imageStride,
                byte* pStates, int imageWidth, int imageHeight)
        {
            for (var yOffset = 0; yOffset < imageHeight; yOffset++)
            {
                var pCurrentPixel = pScan0 + yOffset * imageStride;
                var pCurrentState = pStates + yOffset * imageWidth;
                for (var xOffset = 0; xOffset < imageWidth; xOffset++)
                {
                    int b = pCurrentPixel[0];
                    int g = pCurrentPixel[1];
                    int r = pCurrentPixel[2];
                    var lum = (Math.Max(Math.Max(b, g), r) + Math.Min(Math.Min(b, g), r)) / 2;
                    if (lum < 240)
                        *pCurrentState = (byte)PixelState.None;
                    else if (lum < 250)
                        *pCurrentState = (byte)PixelState.Candidate;
                    else
                        *pCurrentState = (byte)PixelState.Seed;
#if DEBUG
                    if (lum < 240)
                        *(uint*)pCurrentPixel = GraphicsUtil.BLACK; 
                    else if (lum < 250)
                        *(uint*)pCurrentPixel = GraphicsUtil.GRAY;
                    else
                        *(uint*)pCurrentPixel = GraphicsUtil.WHITE;
#endif
                    pCurrentPixel += 4;
                    pCurrentState++;
                }
            }
        }

        /// <summary>
        /// filter the image by hue for frisbee detection
        /// </summary>
        /// <param name="pScan0">pointer to the first pixel of the image</param>
        /// <param name="stride">image stride</param>
        /// <param name="imageWidth">image width</param>
        /// <param name="imageHeight">image height</param>
        /// <param name="pStates">pointer to the array of pixel states</param>
        public static void FilterHue(
                byte* pScan0, int stride, int imageWidth, int imageHeight,
                byte* pStates)
        {
            for (var yOffset = 0; yOffset < imageHeight; yOffset++)
            {
                var pCurrentPixel = pScan0 + yOffset * stride;
                var pCurrentState = pStates + yOffset * imageWidth;
                for (var xOffset = 0; xOffset < imageWidth; xOffset++)
                {
                    int b = pCurrentPixel[0];
                    int g = pCurrentPixel[1];
                    int r = pCurrentPixel[2];
                    var max = Math.Max(Math.Max(b, g), r);
                    var min = Math.Min(Math.Min(b, g), r);
                    var hue = (int)(Math.Atan2(Math.Sqrt(3) * (g - b), (2 * r - g - b)) * 180 / 3.14);
                    if (hue < 0)
                        hue += 360;
                    var lum = (max + min) / 2;
                    var chroma = max - min;
                    var sat = chroma / (255 - Math.Abs(2 * lum - 255.0));
                    if ((hue < 15 || hue > 345) && sat > .5)
                        *pCurrentState = (byte)PixelState.Red;
                    else if (hue > 225 && hue < 255 && sat > .5)
                        *pCurrentState = (byte)PixelState.Blue;
                    else if (r > 191 && b > 192 && g > 192)
                        *pCurrentState = (byte)PixelState.White;
                    else
                        *pCurrentState = (byte)PixelState.None;
#if DEBUG
                    if ((hue < 15 || hue > 345) && sat > .5)
                        *(uint*)pCurrentPixel = GraphicsUtil.RED;
                    else if (hue > 225 && hue < 255 && sat > .5)
                        *(uint*)pCurrentPixel = GraphicsUtil.BLUE;
                    else if (r > 191 && b > 192 && g > 192)
                        *(uint*)pCurrentPixel = GraphicsUtil.WHITE;
                    else
                        *(uint*)pCurrentPixel = GraphicsUtil.BLACK;
#endif
                    pCurrentPixel += 4;
                    pCurrentState++;
                }
            }
        }
    }
}
