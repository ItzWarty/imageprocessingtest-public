﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ImageProcessing
{
    unsafe class FrisbeeFinder
    {
        /// <summary>
        /// the width of the frisbee in inches
        /// </summary>
        public const double FRISBEE_WIDTH = 11;

        /// <summary>
        /// Find frisbees and draw their bounding boxes (broken)
        /// </summary>
        /// <param name="pScan0">pointer to the first pixel of the image</param>
        /// <param name="stride">image stride</param>
        /// <param name="imageWidth">image width</param>
        /// <param name="imageHeight">image height</param>
        public static void FindFrisbees(
                byte* pScan0,
                int stride,
                int imageWidth,
                int imageHeight)
        {
            List<List<Point>> rBlobs;
            List<List<Point>> bBlobs;
            List<List<Point>> wBlobs;

            var states = new byte[imageWidth * imageHeight];

            fixed (byte* pStates = states)
            {
                //fill states
                FilterUtil.FilterHue(pScan0, stride, imageWidth, imageHeight, pStates);

                //find blobs of red, blue, and white
                PixelTester.PixelTest test = PixelTester.TestRedPixel;
                PixelFiller.PixelFill fill = PixelFiller.FillColoredPixel;

                rBlobs = ProcessingUtil.FindBlobs(pStates, imageWidth, imageHeight, fill, test);
                test = PixelTester.TestBluePixel;
                bBlobs = ProcessingUtil.FindBlobs(pStates, imageWidth, imageHeight, fill, test);
                test = PixelTester.TestWhitePixel;
                wBlobs = ProcessingUtil.FindBlobs(pStates, imageWidth, imageHeight, fill, test);
            }

            //draw bounding boxes
            //            var a = false;
            //            foreach (var blob in wBlobs) {
            //                a = !a;
            //                foreach (Point point in blob)
            //                    *(uint*)(pScan0 + point.Y*stride + point.X*4) = a ? 0xFFFF7777 : 0xFF77FF77;
            //            }

            Console.WriteLine(wBlobs.Count);
            Console.WriteLine("WHITE");
            foreach (var blob in wBlobs)
            {
                var minX = imageWidth;
                var maxX = 0;
                //                int minY = imageHeight;
                //                int maxY = 0;
                foreach (int x in blob.Select(point => point.X))
                {
                    //                    int y = point.Y;
                    if (x < minX)
                        minX = x;
                    if (x > maxX)
                        maxX = x;
                }
                var width = maxX - minX;
                var theta = ((double)width / imageWidth) * GraphicsUtil.H_FOV;
                var dist = FRISBEE_WIDTH / (2 * Math.Tan(theta / 2));
                var yposp = (maxX + minX) / 2 - imageWidth / 2;
                var ypos = FRISBEE_WIDTH * width / yposp;
                var xpos = Math.Sqrt(dist * dist - (ypos * ypos + GraphicsUtil.GREEN_CAMERA_HEIGHT * GraphicsUtil.GREEN_CAMERA_HEIGHT));
                var zpos = 0.0;
                //Console.WriteLine(dist);
                //Console.WriteLine(ypos);
                Console.WriteLine(xpos + " " + ypos + " " + zpos);
            }
            Console.WriteLine("RED");
            foreach (var blob in rBlobs)
            {
                var minX = imageWidth;
                var maxX = 0;
                //                int minY = imageHeight;
                //                int maxY = 0;
                foreach (int x in blob.Select(point => point.X))
                {
                    //                    int y = point.Y;
                    if (x < minX)
                        minX = x;
                    if (x > maxX)
                        maxX = x;
                }
                var width = maxX - minX;
                var theta = ((double)width / imageWidth) * GraphicsUtil.H_FOV;
                var dist = FRISBEE_WIDTH / (2 * Math.Tan(theta / 2));
                var yposp = (maxX + minX) / 2 - imageWidth / 2;
                var ypos = FRISBEE_WIDTH * width / yposp;
                var xpos = Math.Sqrt(dist * dist - (ypos * ypos + GraphicsUtil.GREEN_CAMERA_HEIGHT * GraphicsUtil.GREEN_CAMERA_HEIGHT));
                var zpos = 0.0;
                //Console.WriteLine(dist);
                //Console.WriteLine(ypos);
                Console.WriteLine(xpos + " " + ypos + " " + zpos);
            }
            Console.WriteLine("BLUE");
            foreach (var blob in bBlobs)
            {
                var minX = imageWidth;
                var maxX = 0;
                //                int minY = imageHeight;
                //                int maxY = 0;
                foreach (int x in blob.Select(point => point.X))
                {
                    //                    int y = point.Y;
                    if (x < minX)
                        minX = x;
                    if (x > maxX)
                        maxX = x;
                }
                var width = maxX - minX;
                var theta = ((double)width / imageWidth) * GraphicsUtil.H_FOV;
                var dist = FRISBEE_WIDTH / (2 * Math.Tan(theta / 2));
                var yposp = (maxX + minX) / 2 - imageWidth / 2;
                var ypos = FRISBEE_WIDTH * width / yposp;
                var xpos = Math.Sqrt(dist * dist - (ypos * ypos + GraphicsUtil.GREEN_CAMERA_HEIGHT * GraphicsUtil.GREEN_CAMERA_HEIGHT));
                var zpos = 0.0;
                //Console.WriteLine(dist);
                //Console.WriteLine(ypos);
                Console.WriteLine(xpos + " " + ypos + " " + zpos);
            }
        }
    }
}
